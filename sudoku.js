function cellToBox(row, col) {
    return Math.floor(row / 3) * 3 + Math.floor(col / 3);
}

function diff(arr1, arr2) {
    return arr1.filter(function(item) {
        return !arr2.includes(item);
    });
}

function Solver() {
    this.board = [
        // [0, 0, 8, 0, 0, 7, 0, 0, 0],
        // [0, 4, 2, 0, 0, 5, 0, 0, 0],
        // [0, 0, 0, 0, 0, 0, 0, 0, 0],

        // [0, 0, 3, 0, 0, 6, 8, 0, 1],
        // [0, 0, 0, 0, 0, 0, 0, 0, 6],
        // [9, 0, 0, 0, 0, 0, 0, 0, 0],

        // [0, 8, 0, 1, 3, 0, 4, 7, 0],
        // [0, 0, 0, 0, 9, 0, 0, 0, 0],
        // [0, 1, 0, 0, 0, 0, 0, 0, 0],

        [0, 1, 0, 0, 0, 0, 5, 0, 4],
        [0, 9, 6, 0, 0, 7, 0, 0, 0],
        [0, 0, 0, 2, 0, 0, 0, 1, 0],

        [0, 0, 0, 0, 0, 0, 8, 0, 7],
        [0, 8, 5, 0, 6, 0, 0, 0, 2],
        [0, 0, 4, 0, 0, 0, 0, 0, 0],

        [0, 3, 0, 0, 0, 0, 0, 9, 0],
        [0, 0, 9, 0, 3, 0, 0, 0, 5],
        [0, 0, 0, 5, 4, 0, 0, 6, 0],

        // [0, 0, 0, 8, 2, 0, 0, 9, 0],
        // [0, 5, 7, 6, 0, 9, 0, 1, 3],
        // [0, 8, 4, 0, 3, 1, 0, 0, 0],

        // [0, 7, 8, 0, 6, 0, 4, 5, 0],
        // [0, 0, 9, 1, 0, 0, 0, 0, 6],
        // [5, 6, 0, 3, 0, 0, 9, 8, 0],

        // [8, 3, 0, 4, 0, 6, 0, 0, 0],
        // [0, 0, 5, 0, 1, 8, 0, 0, 0],
        // [1, 0, 6, 7, 5, 0, 2, 0, 0],
    ];
    this.candidates = [];
    for(let row = 0; row < 9; row++) {
        this.candidates[row] = [];
        for (let col = 0; col < 9; col++) {
            this.candidates[row][col] = [];
        }
    }

    this.computeCandidates();
}

Solver.prototype.computeCandidates = function () {
    let self = this;
    this.iterateAll(function (row, col) {
        if (self.board[row][col] > 0) {
            self.candidates[row][col] = [];
            return;
        }

        let boxIndex = cellToBox(row, col);
        let boxDigits = self.boxDigits(boxIndex);
        let colDigits = self.colDigits(col);
        let rowDigits = self.rowDigits(row);

        self.candidates[row][col] = diff([1, 2, 3, 4, 5, 6, 7, 8, 9], [...new Set([].concat(boxDigits, colDigits, rowDigits))]);
    });
}

Solver.prototype.removeCandidate = function (row, col, digit) {
    this.candidates[row][col] = this.candidates[row][col].filter(function(item) {
        return item !== digit;
    });
}

Solver.prototype.fillDigit = function (row, col, digit) {
    this.board[row][col] = digit;
    if (digit === 0) {
        // Why this is called is because user can input 0, which clears a cell, so some candidates must be readded, and for now we just lazily readd all candidates
        // Todo this is not great, because it might re-add already removed candidates.
        this.computeCandidates();
        return;
    }
    this.candidates[row][col] = [];
    let box = cellToBox(row, col);
    for(let cell of [].concat(this.boxSiblings(box), this.rowSiblings(row), this.colSiblings(col))) {
        this.removeCandidate(cell.row, cell.col, digit);
    }
}

Solver.prototype.applySolution = function (solution) {
    if (solution.type === 1) {
        this.fillDigit(solution.row, solution.col, solution.digit);
    } else if(solution.type === 2) {
        for (let cell of solution.cells) {
            this.candidates[cell.row][cell.col] = solution.candidates;
        }
    } else if(solution.type === 3) {
        for (let rc of solution.rcandidates) {
            let index = this.candidates[rc.row][rc.col].indexOf(rc.candidate);
            let candidates = this.candidates[rc.row][rc.col].splice(index, 1);
        }
    }
}

Solver.prototype.boxSiblings = function (box) {
    let cells = [];
    for (let row = Math.floor(box / 3) * 3; row < Math.floor(box / 3) * 3 + 3; row++) {
        for (let col = box % 3 * 3; col < box % 3 * 3 + 3; col++) {
            cells.push({col, row});
        }
    }

    return cells;
}

Solver.prototype.boxColumns = function (box) {
    let columns = [];
    for (let col = box % 3 * 3; col < box % 3 * 3 + 3; col++) {
        columns.push(col);
    }

    return columns;
}

Solver.prototype.boxRows = function (box) {
    let rows = [];
    for (let row = Math.floor(box / 3) * 3; row < Math.floor(box / 3) * 3 + 3; row++) {
        rows.push(row);
    }

    return rows;
}

Solver.prototype.rowSiblings = function (row) {
    let cells = [];
    for (let col = 0; col < 9; col++) {
        cells.push({row, col});
    }

    return cells;
}

Solver.prototype.colSiblings = function (col) {
    let cells = [];
    for (let row = 0; row < 9; row++) {
        cells.push({row, col});
    }

    return cells;
}

Solver.prototype.boxDigits = function (box) {
    let digits = [];
    for(let cell of this.boxSiblings(box)) {
        if (this.board[cell.row][cell.col] === 0) {
            continue;
        }
        digits.push(this.board[cell.row][cell.col]);
    }

    return digits;
}

Solver.prototype.rowDigits = function (row) {
    let digits = [];
    for(let cell of this.rowSiblings(row)) {
        if (this.board[cell.row][cell.col] === 0) {
            continue;
        }
        digits.push(this.board[cell.row][cell.col]);
    }

    return digits;
}

Solver.prototype.colDigits = function (colIndex) {
    let digits = [];
    for(let cell of this.colSiblings(colIndex)) {
        if (this.board[cell.row][cell.col] === 0) {
            continue;
        }
        digits.push(this.board[cell.row][cell.col]);
    }

    return digits;
}

Solver.prototype.iterateAll = function (f) {
    for (let row = 0; row < 9; row++) {
        for (let col = 0; col < 9; col++) {
            f(row, col);
        }
    }
};

Solver.prototype.iterateEmpty = function (f) {
    for (let row = 0; row < 9; row++) {
        for (let col = 0; col < 9; col++) {
            if (this.board[row][col] !== 0) {
                continue;
            }
            f(row, col);
        }
    }
};

Solver.prototype.techniques = function () {
    let start = new Date();
    let techniques = [
        {
            name: 'Simple',
            tech: [
                this.scanNakedSingle(),
                this.scanBoxHiddenSingle(),
                this.scanColHiddenSingle(),
                this.scanRowHiddenSingle(),
            ],
        },
        {
            name: 'Candidate elimination',
            tech: [
                this.scanHiddenPairs('box'),
                this.scanHiddenPairs('col'),
                this.scanHiddenPairs('row'),
                this.scanHiddenTriples('box'),
                this.scanHiddenTriples('col'),
                this.scanHiddenTriples('row'),
                this.scanLockedCandidates('col'),
                this.scanLockedCandidates('row'),
            ],
        },
    ];

    let end = new Date();
    console.log(`Scans took ${end - start}ms`);

    return techniques;
}

Solver.prototype.scanNakedSingle = function () {
    let self = this;
    let solutions = [];
    this.iterateEmpty(function (row, col) {
        if (self.candidates[row][col].length === 1) {
            let digit = self.candidates[row][col][0];
            solutions.push({
                row,
                col,
                digit,
                type: 1,
            });
        }
    });

    return {
        name: 'Naked single',
        solutions,
    }
}

Solver.prototype.scanBoxHiddenSingle = function () {
    let self = this;
    let solutions = [];
    this.iterateEmpty(function (row, col) {
        let boxIndex = cellToBox(row, col);
        let boxCandidateCounts = [];
        for (let cell of self.boxSiblings(boxIndex)) {
            let candidates = self.candidates[cell.row][cell.col];

            for (let boxCandidate of candidates) {
                if (typeof boxCandidateCounts[boxCandidate] === 'undefined') {
                    boxCandidateCounts[boxCandidate] = 0;
                }
                boxCandidateCounts[boxCandidate]++;
            }
        }

        for (let digit of self.candidates[row][col]) {
            if (boxCandidateCounts[digit] === 1) {
                solutions.push({
                    row,
                    col,
                    digit,
                    type: 1,
                });
            }
        }
    });

    return {
        name: 'Box hidden single',
        solutions,
    }
}

Solver.prototype.scanColHiddenSingle = function () {
    let self = this;
    let solutions = [];
    this.iterateEmpty(function (row, col) {
        let colCandidateCounts = [];
        for (let cell of self.colSiblings(col)) {
            let candidates = self.candidates[cell.row][cell.col];

            for (let colCandidate of candidates) {
                if (typeof colCandidateCounts[colCandidate] === 'undefined') {
                    colCandidateCounts[colCandidate] = 0;
                }
                colCandidateCounts[colCandidate]++;
            }
        }

        for (let digit of self.candidates[row][col]) {
            if (colCandidateCounts[digit] === 1) {
                solutions.push({
                    row,
                    col,
                    digit,
                    type: 1,
                });
            }
        }
    });

    return {
        name: 'Column hidden single',
        solutions,
    }
}

Solver.prototype.scanRowHiddenSingle = function () {
    let self = this;
    let solutions = [];
    this.iterateEmpty(function (row, col) {
        let rowCandidateCounts = [];
        for (let cell of self.rowSiblings(row)) {
            let candidates = self.candidates[cell.row][cell.col];

            for (let rowCandidate of candidates) {
                if (typeof rowCandidateCounts[rowCandidate] === 'undefined') {
                    rowCandidateCounts[rowCandidate] = 0;
                }
                rowCandidateCounts[rowCandidate]++;
            }
        }

        for (let digit of self.candidates[row][col]) {
            if (rowCandidateCounts[digit] === 1) {
                solutions.push({
                    row,
                    col,
                    digit,
                    type: 1,
                });
            }
        }
    });

    return {
        name: 'Row hidden single',
        solutions,
    }
}

function findPair(candidates) {
    for (let i = 0; i < 9; i++) {
        for (let j = i + 1; j < 10; j++) {
            let a = candidates[i];
            let b = candidates[j];
            if (a.length === 2 && b.length === 2 && JSON.stringify(a) === JSON.stringify(b)) {
                return {
                    candidates: [i, j],
                    cells: a,
                    type: 2,
                };
            }
        }
    }
    return null;
};

function findTriple(candidates) {
    for (let i = 0; i < 8; i++) {
        for (let j = i + 1; j < 9; j++) {
            for (let k = j + 1; k < 10; k++) {
                let a = candidates[i];
                let b = candidates[j];
                let c = candidates[k];
                if (a.length === 3 && b.length === 3 && c.length === 3 && JSON.stringify(a) === JSON.stringify(b) && JSON.stringify(b) === JSON.stringify(c)) {
                    return {
                        candidates: [i, j, k],
                        cells: a,
                        type: 2,
                    };
                }
            }
        }
    }
    return null;
};

Solver.prototype.getUnitCandidateCounts = function(unit, index) {
    let candidateCounts = [];
    for (let i = 0; i <= 9; i++) {
        candidateCounts[i] = [];
    }

    let siblings;
    if (unit === 'box') {
        siblings = this.boxSiblings(index);
    } else if (unit === 'row') {
        siblings = this.rowSiblings(index);
    } else if (unit === 'col') {
        siblings = this.colSiblings(index);
    }

    for (let cell of siblings) {
        if (this.board[cell.row][cell.col] > 0) {
            continue;
        }
        for (let candidate of this.candidates[cell.row][cell.col]) {
            candidateCounts[candidate].push(cell);
        }
    }

    return candidateCounts;
}

Solver.prototype.scanHiddenPairs = function (unit) {
    let solutions = [];
    let self = this;

    for (let index = 0; index < 9; index++) {
        let candidates = this.getUnitCandidateCounts(unit, index);
        let result = findPair(candidates);

        if (result) {
            let candidatesToRemove = result.cells.filter(function (cell) {
                return self.candidates[cell.row][cell.col].length > 2;
            }).length > 0;
            if (candidatesToRemove) {
                solutions.push(result);
            }
        }
    }

    return {
        name: unit.charAt(0).toUpperCase() + unit.slice(1) + ' Hidden Pairs',
        solutions,
    };
};

Solver.prototype.scanHiddenTriples = function (unit) {
    let solutions = [];
    let self = this;

    for (let index = 0; index < 9; index++) {
        let candidates = this.getUnitCandidateCounts(unit, index);
        let result = findTriple(candidates);

        if (result) {
            let candidatesToRemove = result.cells.filter(function (cell) {
                return self.candidates[cell.row][cell.col].length > 2;
            }).length > 0;
            if (candidatesToRemove) {
                solutions.push(result);
            }
        }
    }

    return {
        name: unit.charAt(0).toUpperCase() + unit.slice(1) + ' Hidden Triples',
        solutions,
    };
};

Solver.prototype.scanLockedCandidates = function (unit) {
    // Todo this could be optimized by using precomputed lookup tables
    let solutions = [];
    for (let box of [0, 1, 2, 3, 4, 5, 6, 7, 8]) {
        let candidateOccurrences = {};
        for (let cell of this.boxSiblings(box)) {
            for (let candidate of this.candidates[cell.row][cell.col]) {
                if (typeof candidateOccurrences[candidate] === 'undefined') {
                    candidateOccurrences[candidate] = [];
                }
                if (unit === 'col') {
                    candidateOccurrences[candidate].push(cell.col);
                } else {
                    candidateOccurrences[candidate].push(cell.row);
                }
            }
        }

        for (let candidate of Object.keys(candidateOccurrences)) {
            let occurrences = candidateOccurrences[candidate];
            candidate = parseInt(candidate);

            // It's basically a hidden single, so don't let it show up in locked candidate list
            if (occurrences.length < 2) {
                continue;
            }

            // Candidate shows up in more than one column or row in the box, so it's not useful
            if ((new Set(occurrences)).size !== 1) {
                continue;
            }

            // Finally we check if this candidate is present in other boxes of the same col / row, or in other words are there any candidates to remove
            let siblings;
            let rcandidates = [];
            if (unit === 'col') {
                siblings = this.colSiblings(occurrences[0]);
            } else {
                siblings = this.rowSiblings(occurrences[0]);
            }
            for (let cell of siblings) {
                for (let checkcandidate of this.candidates[cell.row][cell.col]) {
                    if (checkcandidate === candidate && box !== cellToBox(cell.row, cell.col)) {
                        rcandidates.push({
                            candidate,
                            row: cell.row,
                            col: cell.col
                        });
                    }
                }
            }

            if (rcandidates.length > 0) {
                solutions.push({
                    unit,
                    candidate,
                    rcandidates,
                    index: occurrences[0],
                    type: 3,
                });
            }
        }
    }

    return {
        name: unit.charAt(0).toUpperCase() + unit.slice(1) + ' Locked candidates',
        solutions,
    };
}